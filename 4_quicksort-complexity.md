# 4 "QuickSort complexity"

QuickSort is a sorting algorithm that doesn't require any temporary support array list, the sorting is done on the original array list. A pivot element is selected and greater elements than pivot are moved to the right of pivot and the lesser elements than pivot are moved to the left of pivot, until all the elements are sorted.
