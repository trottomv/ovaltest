# 2 "Nontechnical Explanation"

## Episode 1

A colleague who has just finished an Excel course calls me at phone and asks me how to sum a column of hours calculated per line, he was trying on his own but the result was not the desired one.
On two feet I didn't know how to solve it because not remembering the formula for sum hours, so I suggest him take a look at Microsoft's Excel Support looking for "sum hours", and send me the file via e-mail that I would have given him a look as soon as I could.
Shortly after he writes to me that he has solved it by himself by finding the solution on Microsoft's Excel support.


## Episode 2

A friend on holiday in Sicily calls me telling that he could no longer surf the internet from his pc, he usually used 4g portable hotspots but the pc connected him to the wifi of the hotel to which he automatically connected but required authentication to the portal and he could no longer navigate. I suggest turning off and reactivating the wifi and eliminating the wifi network of the hotel from the stored connections, with some small and simple telephone directions then resolved the problem.
