# OVALTEST

$ `pip install -r requirements.txt`

## 1 "SpamEgg"

$ `python 1_spamegg.py`

## 2 "Nontechnical Explanation"

[2_nontechnical-explanation.md](2_nontechnical-explanation.md)

## 3 "Factorial"

$ `python 3_factorial.py -h`

```bash
usage: 3_factorial.py [-h] [-n NUMBER]

optional arguments:
  -h, --help                    show this help message and exit
  -n NUMBER, --number NUMBER    enter a number to calculate the factorial

```

$ `python 3_factorial.py -n NUMBER`

## 4 "QuickSort complexity"

[4_quicksort-complexity.md](4_quicksort-complexity.md)

## 5 "Python dislike"

[5_python-dislike.md](5_python-dislike.md)


## 6 "Evil Remove"

$ `touch JaVa jAvA a.java b.Java test/JaVA test/test.java`

$ `/bin/sh 6_evil-remove.sh`

## 7 "Vincent & Desta Word Game"

### SOLUTION Help

$ `python 7_word-game.py -h`

```bash
usage: 7_word-game.py [-h] [-t TEST]

*** Vincent & Desta Word Game ***

optional arguments:
  -h, --help            show this help message and exit
  -t TEST, --test TEST  Enter value of test set [1 or 2]
```

### SOLUTION Test set 1

$ `python 7_word-game.py -t 1`


### SOLUTION Test set 2

$ `python 7_word-game.py -t 2`
