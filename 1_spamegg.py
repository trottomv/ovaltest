def multiples(m, count):
    arr = []
    for i in range(count):
            arr.append(i*m)
    return arr

y = [x for x in range(1,21)]
z = [0 for x in range(20)]


for (index, value) in enumerate(y):
    z[index] = value
    if value in multiples(3,7):
            z[index] = "Spam"
    if value in multiples(5,5):
            z[index] = "Egg"
    if value in multiples(5,5) and value in multiples(3,7):
            z[index] = "SpamEgg"

for i in z:
    print(i)
