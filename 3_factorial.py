import sys
import argparse


class Factorial():

    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('-n', '--number', type = int, help = 'enter a number to calculate the factorial')
        self.args = self.parser.parse_args()


    def calc_factorial(self, number):
        """This is a recursive function
        to find the factorial of an integer"""

        if number == 1:
            return 1
        else:
            return (number * self.calc_factorial(number-1))

    def run(self):
        if not self.args.number:
            self.parser.print_help(sys.stderr)
            sys.exit()
        else:
            print("%s! = %s" % (self.args.number, self.calc_factorial(self.args.number)))

if __name__ == '__main__':

    factorial = Factorial()
    factorial.run()
