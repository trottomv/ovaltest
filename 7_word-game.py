import numpy as np
import random
import string
import argparse
import sys
import operator as op
from functools import reduce


class WordGame():

    def __init__(self):
        self.parser = argparse.ArgumentParser(description = '*** Vincent & Desta Word Game ***')
        self.parser.add_argument('-t', '--test', type = int, help = 'Enter value of test set [1 or 2] ')
        self.args = self.parser.parse_args()

        self.matrix = []

    def inputfile(self):
        input_file = open('test/2018-7.in')
        lines = input_file.readlines()
        T = int(lines[0].rstrip())
        cases = []
        self.matrix = []

        for index, line in enumerate(lines[1:]):
            try:
                if int(line.rstrip().split(' ')[0]):
                    cases.append(line.rstrip().split())
                    listword = []
                    lenword = int(line.rstrip().split(' ')[1])
                    for word in lines[index+2:index+2+int(line.rstrip().split(' ')[0])]:
                        a = list(word.rstrip())
                        listword.append(a)
                    self.matrix.append(listword)

            except Exception as e:
                pass

    def combinations(self, n, r):
        r = min(r, n-r)
        numer = reduce(op.mul, range(n, n-r, -1), 1)
        denom = reduce(op.mul, range(1, r+1), 1)
        return numer / denom

    def wordgenerator(self):
        T = random.randint(1,100)
        self.matrix = []

        for ncase in range(T):

            case = []
            L = random.randint(1,10)
            N = random.randint(1,2000)
            casecombinations = self.combinations(len(string.ascii_uppercase), L)

            if N > casecombinations:
                N = casecombinations

            while True:
                word = [random.choice(string.ascii_uppercase) for x in range(L)]
                if word not in case:
                    case.append(word)
                if len(case) == N:
                    break

            self.matrix.append(case)


    def run(self):
        if not self.args.test or self.args.test not in [1, 2] :
            self.parser.print_help(sys.stderr)
            sys.exit()
        if self.args.test == 1:
            self.inputfile()
        if self.args.test == 2:
            self.wordgenerator()
        self.solutionmatrix = self.matrix
        allcasesolutions = []
        for index, case in enumerate(self.solutionmatrix):
            case = np.array(case)
            case = np.transpose(case)
            for word in case:
                random.shuffle(word)
            case = np.transpose(case)
            solutions = []

            for word in case.tolist():
                if word in self.matrix[index] or word in allcasesolutions:
                    pass
                else:
                    solutions.append(word)

            if len(solutions) > 0:
                casesolution = ''.join(random.choice(solutions))
                allcasesolutions.append(casesolution)
                print("Case #%s: %s" % (index+1, casesolution))
            else:
                print("Case #%s: -" % (index+1))


if __name__ == '__main__':

    wordgame = WordGame()
    wordgame.run()
